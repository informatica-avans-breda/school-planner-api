const express = require('express')
const router = express.Router()

const FollowController = require('../controllers/follow.controller')

router.post('/users/:id/follow', FollowController.follow)

router.get('/users/:id/followers', FollowController.followers)

router.get('/users/:id/following', FollowController.following)

router.delete('/users/:id/unfollow/:thisUserId', FollowController.unfollow)

module.exports = router