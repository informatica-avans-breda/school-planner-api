require('dotenv').config()

const chai = require('chai');
const should = chai.should()
const jwt = require('jsonwebtoken')

const requester = require('../requester.spec');

const User = require('../../src/models/user.model')()

describe('auth routes', function() {
    describe('register', function() {
        it('POST /api/register should create new user', async function() {
            const res = await requester.post('/api/register').send({
                email: 'test@test.nl',
                firstName: 'test',
                lastName: 'test',
                password: 'sercet'
            }) 

            res.status.should.equal(201)

            const user = await User.findOne({ email: 'test@test.nl' })
            user.should.have.property('firstName').that.equals('test')

        })

        it('POST /api/register should reject with no email', async function() {
            const res = await requester.post('/api/register').send({
                firstName: 'test',
                lastName: 'test',
                password: 'sercet'
            }) 

            res.status.should.equal(400)
            res.body.message.should.equal('user validation failed: email: Een gebruiker moet een emailadres hebben.')         
        })
    })

    describe('login', function() {
        xit('POST /api/login should log in and return JWT token', async function() {
            await createMockUser()

            const res = await requester.post('/api/login').send({
                email: 'test@test.nl',
                password: 'secret'
            })

            res.should.have.status(200)
            res.body.should.have.property('token')
            res.body.should.have.property('expiresIn')
            res.body.user.should.have.property('firstName').that.equals('test')

        })

        it('POST /api/login should log in and return JWT token', async function() {
            await createMockUser()

            const res = await requester.post('/api/login').send({
                email: 'tes@test.nl',
                password: 'secret'
            })

            res.status.should.equal(401)
            res.body.message.should.equal('Auth failed, no user found with email.')
        })
    })
})

async function createMockUser() {
    await requester.post('/api/auth/register')
      .send({
        email: 'test@test.nl',
        firstName: 'test',
        lastName: 'test',
        password: 'secret'
      })
  }
  