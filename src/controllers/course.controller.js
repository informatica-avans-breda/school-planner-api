const Course = require('../models/course.model')()
const Teacher = require('../models/teacher.model')()
const User = require('../models/user.model')()

exports.create = async (req, res, next) => {
    const course = new Course({
        courseName: req.body.courseName,
        courseCode: req.body.courseCode,
        credits: req.body.credits,
        semester: req.body.semester,
        teacher: req.body.teacher.id,
        lessons: req.body.lessons,
        creator: req.body.creator
    })
    
    const createdCourse = await course.save()

    res.status(201).json({
        message: 'Course created succesfully!',
        course: {
            ...createdCourse,
            id: createdCourse._id
        }
    })
}

exports.getByUserId = async (req, res, next) => {
    const user = await User.findById(req.params.id)
    const courses = await Course.find({ creator : user })

    res.status(200).json({
        message: 'Courses retrieved succesfully!',
        user: user,
        courses: courses
    })
}

exports.update = async (req, res, next) => {
    course = await Course.findById(req.params.id)
    
    course.courseName = req.body.courseName,
    course.courseCode = req.body.courseCode,
    course.credits = req.body.credits,
    course.semester = req.body.semester,
    course.teacher = req.body.teacher,
    course.lessons = req.body.lessons,
    course.creator = req.body.creator
    // course = {
    //     courseName: req.body.courseName,
    //     courseCode: req.body.courseCode,
    //     credits: req.body.credits,
    //     semester: req.body.semester,
    //     teacher: req.body.teacher.id,
    //     lessons: req.body.lessons,
    //     creator: req.body.creator
    // }
    await course.save()
    // await Course.findByIdAndUpdate(req.params._id, course)
    res.status(200).json({ 
        message: 'Course updated succesfully!',
        result: course
    })
}