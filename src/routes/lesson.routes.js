const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')

const lessonController = require('../controllers/lesson.controller')

router.post('/courses/:id/lessons', lessonController.create)

router.get('/courses/:id/lessons', lessonController.getAll)

router.get('/courses/:id/lessons/:lessonId', lessonController.getById)

router.put('/courses/:id/lessons/:lessonId', lessonController.update)

router.delete('/courses/:id/lessons/:lessonId', lessonController.delete)

module.exports = router
