const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')

const Course = require('../models/course.model')()
const CrudController = require('../controllers/crud')
const courseController = require('../controllers/course.controller')
const courseCrudController = new CrudController(Course)

router.post('/', courseController.create)

router.get('/', courseCrudController.getAll)

router.get('/:id', courseCrudController.getOne)

router.get('/user/:id', courseController.getByUserId)

router.put('/:id', courseController.update)

router.delete('/:id', courseCrudController.delete)

module.exports = router
