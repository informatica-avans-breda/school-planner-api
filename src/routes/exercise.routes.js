const express = require('express')
const router = express.Router()

const exerciseController = require('../controllers/exercise.controller')

router.post('/courses/:id/lessons/:lessonId/exercises', exerciseController.create)

router.get('/courses/:id/lessons/:lessonId/exercises', exerciseController.getAll)

router.get('/courses/:id/lessons/:lessonId/exercises/:exerciseId', exerciseController.getById)

router.put('/courses/:id/lessons/:lessonId/exercises/:exerciseId', exerciseController.update)

router.delete('/courses/:id/lessons/:lessonId/exercises/:exerciseId', exerciseController.delete)

module.exports = router
