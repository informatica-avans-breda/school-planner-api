const express = require('express')
const router = express.Router()

const User = require('../models/user.model')()

const CrudController = require('../controllers/crud')
const UserCrudController = new CrudController(User)

const UserController = require('../controllers/user.controller')

router.get('/', UserCrudController.getAll)

router.get('/:id', UserCrudController.getOne)

router.get('/courses/:id', UserController.getCourses)

router.put('/:id', UserCrudController.update)

router.delete('/:id', UserController.delete)

module.exports = router
