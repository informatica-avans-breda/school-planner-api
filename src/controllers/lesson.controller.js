const Course = require('../models/course.model')()
const Lesson = require('../models/lesson.schema')

const neo = require('../../neo')
const errors = require('../errors')

exports.create = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    course.lessons.push(req.body)
    const result = await course.save()
    res.status(201).json({
        message: 'Successfully added lesson to course!',
        result: result
     })
}

exports.getAll = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    const lessons = course.lessons
    res.status(200).send(lessons)
}

exports.getById = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    const lesson = course.lessons.id(req.params.lessonId)
    
    if(!lesson){
        res.status(404).json({ message: 'Exercise not found.'})
    }
    res.status(200).send(lesson)
}

exports.update = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    const lesson = await course.lessons.id(req.params.lessonId).set(req.body)
    await course.save()
    res.status(200).json({
        message: 'Successfully updated lesson form course!',
        result: lesson
    })
}

exports.delete = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    course.lessons.id(req.params.lessonId).remove()
    course.save()
    res.status(200).json({
        message: 'Successfully deleted lesson form course!'
    })
}