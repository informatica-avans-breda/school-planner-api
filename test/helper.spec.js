require('dotenv').config()

const connect = require('../connect')
const neo = require('../neo')

const User = require('../src/models/user.model')()
const Course = require('../src/models/course.model')()

connect.mongo(process.env.MONGO_TEST_DB)
connect.neo(process.env.NEO4J_TEST_URL, process.env.NEO4J_TEST_PASSWORD)

beforeEach(async () => {
  await Promise.all([
      User.deleteMany(),
      Course.deleteMany()
  ])

  const session = neo.session()
  await session.run(neo.dropAll)
  await session.close()
})
