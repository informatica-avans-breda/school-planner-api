const chai = require('chai')
const expect = chai.expect

const requester = require('../requester.spec')

describe('user journeys', function() {
    xit('create course, create lesson, create exercise', async function() {
        let res

        const course = {
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                _id: '5fd1260fccc26439dca84114',
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }]
        }

        res = await requester.post('/api/courses').send(course)
        expect(res).to.have.status(201)
        course.id = res.body.id

        const lesson = {
            lessonNumber: 1,
            date: '2020-09-02',
            duration: 150,
            subject: 'Introdutie'
        }

        res = await requester.post('/api/course')
    })
})