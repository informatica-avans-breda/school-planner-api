const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ExerciseSchema = require('./exercise.schema')

const LessonSchema = new Schema({
    lessonNumber:{
        type: Number,
        required: [true, 'Een les heeft een lesnummer.'],
        validate: { 
            validator: (lessonNumber) => lessonNumber > 0, 
            message: 'Het lesnummer moet een positief getal zijn.'
        }
    },
    date: {
        type: Date,
        required: [true, 'De les vindt plaats op een datum.']
    },
    duration: {
        type: Number,
        validate: {
            validator: (duration) => duration >= 45 && duration <= 250,
            message: 'Studiepunten moet een positief getal zijn.'
          },
        required: [true, 'Een les heeft een tijdsduur.']
    },
    exercises: {
        type: [ExerciseSchema],
        default: []
    },
    subject: {
        type: String,
        required: [true, 'Elke les heeft een onderwerp.'],
        minlength: 3
    }
}, {
    toObject: {virtuals: true},
    toJSON: {virtuals: true}
})

LessonSchema.virtual('totalExercises').get(function () {
    if(this.exercises.length === 0) {
        return "Er zijn geen opgaven."
    } else {
        return this.exercises.length
    }
})

LessonSchema.virtual('amountOfDone').get(function () {
    if(this.exercises.length === 0) {
        return "Er zijn geen opgaven."
    } else {
        let amountDone = 0
        for(let exercise of this.exercises) {
            if(exercise.isDone == true){
                amountDone++
            }
        }
        return amountDone
    }
})

LessonSchema.virtual('percentageOfDone').get(function () {
    if(this.exercises.length === 0) {
        return "Er zijn geen opgaven."
    } else {
        let per = this.amountDone / this.totalExercises
        return per * 100
    }
})

module.exports = LessonSchema