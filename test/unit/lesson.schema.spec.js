const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Course = require('../../src/models/course.model')()

describe('lesson schema', function() {
    it('should be ok', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.ok
    })

    it('should reject missing lessonNumber', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                date: '2020-09-02',
                duration: 50,
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject negative lessonNumber', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: -1,
                date: '2020-09-02',
                duration: 50,
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing date', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                duration: 50,
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing duration', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when duration is to low', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                duration: 44,
                date: '2020-09-02',
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when duration is to high', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                duration: 256,
                date: '2020-09-02',
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing date', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                duration: 150,
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing subject', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                duration: 150,
                date: '2020-09-02'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when subjectis to short', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                duration: 150,
                date: '2020-09-02',
                subject: 'In'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })
})