const chai = require('chai')
const expect = chai.expect
const jwt = require('jsonwebtoken')

const requester = require('../requester.spec')
const neo = require('../../neo')

const User = require('../../src/models/user.model')()

describe('follow routes', function() {
    describe('follow', function() {
        it('POST /users/:id/follow should follow other user', async function() {
            await createMockUsers()

            const user1 = await User.findOne({ email: 'user1@test.nl' })
            const user2 = await User.findOne({ email: 'user2@test.nl' })

            const res = await requester.post('/api/users/' + user1._id + '/follow').send({ id: user2._id })

            res.status.should.equal(200)

            const session = neo.session()
            const result = await session.run(neo.getFollowRelationship)
            session.close()

            result.records[0].length.should.equal(1)

        })
    })

    describe('unfollow', function() {
        xit('DELETE to /api/users/:id/unfollow/:thisUserId should unfollow another user', async function () {
            await followUser()
    
            const user1 = await User.findOne({ email: 'user1@test.nl' })
            const user2 = await User.findOne({ email: 'user2@test.nl' })
    
            const res = await requester.delete('/api/users/' + user1._id + '/unfollow/' + user2._id)
    
            res.status.should.equal(200)
    
            const session = neo.session()
            const result = await session.run(neo.getFollowRelationship)
            session.close()
    
            result.records.should.be.empty
          })
    
    })
})

async function createMockUsers() {
    await requester.post('/api/register')
      .send({
        email: 'user1@test.nl',
        firstName: 'test2',
        lastName: 'test',
        password: 'secret'
      })
  
    await requester.post('/api/register')
      .send({
        email: 'user2@test.nl',
        firstName: 'test2',
        lastName: 'test',
        password: 'sercet'
      })
  }
  
  async function followUser() {
    await createMockUsers()
  
    const user1 = await User.findOne({ email: 'user1@test.nl' })
    const user2 = await User.findOne({ email: 'user2@test.nl' })
  
    await requester.post('/api/follows/' + user1._id).send({ otherUserId: user2._id })
  }
  