const neo = require('../../neo')
const errors = require('../errors')

const User = require('../models/user.model')()
const Course = require('../models/course.model')()

exports.delete = async (req, res, next) => {
    const user = await User.findById(req.params.id)
    const userId = req.params.id.toString()

    if (user == null) {
        res.status(404).send({
            error: 'There was no user found.'
        })
    } else {
        
        await user.delete()
        res.status(200).send({
            message: 'User is successfully deleted'
        })
    
        const session = neo.session()
        await session.run(neo.deleteUser, {
            userId: userId
        })
        session.close()

    }

}

exports.getCourses = async (req, res, next) => {
    const user = await User.findById(req.params.id)
    const courses = await Course.find({ creator : user })

    res.status(200).json({
        message: 'Courses retrieved succesfully!',
        user: user,
        courses: courses
    })
}