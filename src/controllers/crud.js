const errors = require("../errors")

class CrudController {
  constructor(model) {
    this.model = model
  }

  create = async (req, res, next) => {
    console.log(req.body)
    const entity = new this.model(req.body)
    await entity.save()
    res.status(201).json({id: entity.id})
  }

  getAll = async (req, res, next) => {
    const entities = await this.model.find()
    res.status(200).json({
      result: entities
    })
  }

  getOne = async (req, res, next) => {
    const entity = await this.model.findById(req.params.id)
    res.status(200).send(entity)
  }

  update = async (req, res, next) => {
    const entity = await this.model.findByIdAndUpdate(req.params.id, req.body)
    await entity.save()
    res.status(200).send({
      message: 'Successfully updated!',
      result: entity
    })
  }

  delete = async (req, res, next) => {
    const entity = await this.model.findById(req.params.id)
    await entity.delete()
    res.status(200).send({
      message: 'Successfully deleted!'
    })
    next()
  }
}

module.exports = CrudController
