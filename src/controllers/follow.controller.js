const neo = require('../../neo')
const errors = require('../errors')

const User = require('../models/user.model')()

exports.follow = async (req, res, next) => {
    const otherUserId = req.params.id
    await User.findById(otherUserId)

    const userId = req.body.id
    await User.findById(userId)

    // neo session
    const session = neo.session()
    const result = await session.run(neo.checkFollowing, {
        userId: userId.toString(),
        otherUserId: otherUserId.toString()
    })
    
    if(result.records[0]._fields[0] === true) {
        res.status(400).json({ error: 'You are already following this user!'})
    } else{
        await session.run(neo.followUser, {
            userId: userId,
            otherUserId: otherUserId
        })

        res.status(200).send({
            message: 'Following completed!',
            userYouFollowNow: otherUserId
        })
    }

    session.close()
}

exports.followers = async (req, res, next) => {
    const userId = req.params.id
    await User.findById(userId)

    const session = neo.session()
    const result = await session.run(neo.getFollowers, {
        userId: userId.toString()
    })
    session.close()

    if(extractUsers(result.records).length === 0){
        res.status(200).json({ message: 'You have no followers.'})
    } else {
        res.status(200).json({ following: extractUsers(result.records) })
    }

}

exports.following = async (req, res, next) => {
    const userId = req.params.id
    await User.findById(userId)

    const session = neo.session()
    const result = await session.run(neo.getFollowing, {
        userId: userId.toString()
    })
    session.close()

    if(extractUsers(result.records).length === 0){
        res.status(200).json({ message: 'You do not follow anyone.'})
    } else {
        res.status(200).json({ following: extractUsers(result.records) })
    }

}

exports.unfollow = async (req, res, next) => {
    const otherUserId = req.params.id
    await User.findById(otherUserId)

    const userId = req.params.thisUserId
    await User.findById(userId)

    // neo session
    const session = neo.session()
    const result = await session.run(neo.checkFollowing, {
        userId: userId.toString(),
        otherUserId: otherUserId.toString()
    })
    
    if(result.records[0]._fields[0] === false) {
        res.status(400).json({ error: 'You are not following this user.'})
    } else{
        await session.run(neo.unfollowUser, {
            userId: userId.toString(),
            otherUserId: otherUserId.toString()
        })

        res.status(200).send({
            message: 'Unfollowing completed!',
            userYouFollowNow: otherUserId
        })
    }

    session.close()
}

function extractUsers(records) {
    let users = []
    for (let record of records) {
      users.push(record._fields[0].properties)
    }
    return users;
  }
  