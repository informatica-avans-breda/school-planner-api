const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ExerciseSchema = new Schema({
    exerciseNumber: {
        type: Number,
        required: [true, 'Elke opgave heeft een opgavenummer.'],
        validate: {
            validator: (exerciseNumber) => exerciseNumber > 0,
            message: 'Het opgavenummer moet een positief getal zijn.'
        },
        min: 1
    },
    question: {
        type: String,
        required: [true, 'Elke opgave heeft een vraag of stelling.'],
        minlength: 8
    },
    typeOfExercise: {
        type: String,
        required: [true, 'Een opgave is van een bepaald type.'],
        enum: ['Open vraag', 'Meerkeuze vraag', 'Verslag', 'Programeeropgave']
    },
    dueDate: {
        type: Date,
        min: '2018-08-31'
    },
    isDone: {
        type: Boolean,
        required: [true, 'Van elke opgave moet aangegeven worden of de opgave al af is.']
    }
})

module.exports = ExerciseSchema