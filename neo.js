const neo = require('neo4j-driver')

function connect(url, password) {
    this.driver = neo.driver(
        url,
        neo.auth.basic(process.env.NEO4J_USER, password)
    )
}

function session() {
    return this.driver.session({
        defaultAccessMode: neo.session.WRITE
    })
}

module.exports = {
    connect,
    session,
    dropAll: 'MATCH (n) DETACH DELETE n',
    createUser: 'CREATE (user: USER { id: $userId, email: $userEmail, firstName: $userFirstName, lastName: $userLastName })',
    deleteUser: 'MATCH (n { id: $userId }) DETACH DELETE n',
    followUser: 'MATCH (u:USER { id: $userId }), (ou:USER { id: $otherUserId }) MERGE (u)-[:FOLLOWS]->(ou)',
    unfollowUser: 'MATCH (u:USER { id: $userId })-[r:FOLLOWS]->(ou:USER { id: $otherUserId }) DELETE r',
    checkFollowing: 'RETURN EXISTS( (:USER { id: $userId })-[:FOLLOWS]->(:USER { id: $otherUserId }) )',
    getFollowing: 'MATCH (:USER { id: $userId })-[r:FOLLOWS]->(users:USER) RETURN users',
    getFollowers: 'MATCH (:USER { id: $userId })<-[r:FOLLOWS]-(users:USER) RETURN users',
    getFollowRelationship: 'MATCH (:USER)-[r:FOLLOWS]->(:USER) RETURN r'
}