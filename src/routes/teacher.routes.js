const express = require('express')
const router = express.Router()

const Teacher = require('../models/teacher.model')()
const CrudController = require('../controllers/crud')
const teacherCrudController = new CrudController(Teacher)

router.post('/', teacherCrudController.create)

router.get('/', teacherCrudController.getAll)

router.get('/:id', teacherCrudController.getOne)

router.put('/:id', teacherCrudController.update)

router.delete('/:id', teacherCrudController.delete)

module.exports = router