const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')

const getModel = require('./model_cache')

const UserSchema = new Schema({
  firstName: {
    type: String,
    required: [true, 'Een gebruiker moet een voornaam hebben.']
  },
  lastName: {
    type: String,
    required: [true, 'Een gebruiker moet een achternaam hebben.']
  },
  email: {
    type: String,
    required: [true, 'Een gebruiker moet een emailadres hebben.'],
    unique: [true, 'Het emailadres moet uniek zijn.']
  },
  password: {
    type: String,
    required: [true, 'Wachtwoord is vereist.']
  }
})

UserSchema.plugin(uniqueValidator)

module.exports = getModel('user', UserSchema)
