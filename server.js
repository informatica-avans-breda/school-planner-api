require('dotenv').config()

const connect = require('./connect')
const app = require('./src/app')

const http = require('http');

const port = process.env.PORT || 3000;

app.set('port', port);

const server = http.createServer(app);
server.listen(port);

connect.mongo(process.env.MONGO_PROD_DB)
// connect.neo(process.env.NEO4J_URL, process.env.NEO4J_PASSWORD)
