const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../src/app')

chai.use(chaiHttp)

let requester = chai.request(app).keepOpen()
module.exports = requester

after(function() {
    requester.close()
})