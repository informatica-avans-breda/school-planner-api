const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Course = require('../../src/models/course.model')()

describe('course model', function() {
  it('should be ok', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      courseCode: 'SSWFR',
      credits: 2,
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.ok
  })


  it('should reject a negative credits', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      courseCode: 'SSWFR',
      credits: -1,
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a missing credits', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      courseCode: 'SSWFR',
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a missing semester', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      courseCode: 'SSWFR',
      credits: 2,
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a missing courseName', async function() {
    const fakeCourse = {
      courseCode: 'SSWFR',
      credits: 2,
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a missing courseCode', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      credits: 2,
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a short courseName', async function() {
    const fakeCourse = {
      courseName: 'Se',
      courseCode: 'SSWFR',
      credits: 2,
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a short courseCode', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      courseCode: 'SS',
      credits: 2,
      semester: '2.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })

  it('should reject a semester that is not in th enum', async function() {
    const fakeCourse = {
      courseName: 'Server-side web frameworks',
      courseCode: 'SSWFR',
      credits: 2,
      semester: '4.1',
      teachers: [{
        firstName: 'Johan',
        lastName: 'Smarius',
        email: 'johan.smarius@avans.nl'
      }]
    }

    await expect(new Course(fakeCourse).validate()).to.be.rejectedWith(Error)
  })
})
