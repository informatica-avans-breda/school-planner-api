const jwt = require('jsonwebtoken')

exports.getToken = (req, res, next) => {
    console.log(req.headers.authorization)

    try{
        const token = req.headers.authorization.split(" ")[1]
        const decodedToken = jwt.verify(token, process.env.JWTSECRETKEY)
        req.userData = {email: decodedToken.email, userId: decodedToken.userId}
        next()
    } catch {
        res.status(401).json({ message: 'Auth failed!' })
    }
}

exports.checkId = (req, res, next) => {
    const token = req.header.authorization.split(" ")[1]
    const decodedToken = jwt.verify(token, process.env.JWTSECRETKEY)
    req.userData = {email: decodedToken.email, userId: decodedToken.userId}

    const userId = decodedToken.userId
    const creatorId = req.body.creator

    if (userId === creatorId) {
        next()
    } else {
        res.status(403).send({
            message: 'You are not the creator of this object.'
        })
    }
}

exports.getUserId = (req, res, next) => {
    const token = req.header.authorization.split(" ")[1]
    const decodedToken = jwt.verify(token, process.env.JWTSECRETKEY)
    req.userData = {email: decodedToken.email, userId: decodedToken.userId}

    const userId = decodedToken.userId

    console.log('userId in getUserId check-auth: ', userId)

    return userId
    next()
}