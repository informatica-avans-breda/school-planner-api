const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('../../src/models/user.model')()

describe('user model', function() {
    it('should be ok', async function() {
        const user = new User ({
            firstName: 'Iza',
            lastName: 'Moerenhout',
            email: 'iza.moerenhout@email.nl',
            password: 'sercet123'
        })

        await expect(user.save()).to.be.ok
    })

    it('should reject a missing first name', async function() {
        const user = new User ({
            lastName: 'Moerenhout',
            email: 'iza.moerenhout@email.nl',
            password: 'sercet123'
        })

        await expect(user.save()).to.be.rejectedWith(Error)
    })

    it('should reject a missing last name', async function() {
        const user = new User ({
            firstName: 'Iza',
            email: 'iza.moerenhout@email.nl',
            password: 'sercet123'
        })

        await expect(user.save()).to.be.rejectedWith(Error)
    })

    it('should reject a missing email', async function() {
        const user = new User ({
            firstName: 'Iza',
            lastName: 'Moerenhout',
            password: 'sercet123'
        })

        await expect(user.save()).to.be.rejectedWith(Error)
    })

    it('should reject a missing password', async function() {
        const user = new User ({
            firstName: 'Iza',
            lastName: 'Moerenhout',
            email: 'iza.moerenhout@email.nl'
        })

        await expect(user.save()).to.be.rejectedWith(Error)
    })

    it('should not create a duplicate user', async function() {
        const firstUser = new User ({
            firstName: 'Iza',
            lastName: 'Moerenhout',
            email: 'iza.moerenhout@email.nl',
            password: 'sercet123'
        })
        
        firstUser.save()

        const secondUser = new User ({
            firstName: 'Iza',
            lastName: 'Moerenhout',
            email: 'iza.moerenhout@email.nl',
            password: 'sercet123'
        })

        await expect(secondUser.save()).to.be.rejectedWith(Error)

        let count = await User.find().countDocuments()
        expect(count).to.equal(1)
    })

})