const express = require('express');
require('express-async-errors');

const app = express();

const helmet = require('helmet');
const cors = require('cors');

// Express Middleware
app.use(express.json());
app.use(helmet());
app.use(cors());

const authRoutes = require('./routes/auth.routes');
const courseRoutes = require('./routes/course.routes');
const lessonRoutes = require('./routes/lesson.routes');
const exerciseRoutes = require('./routes/exercise.routes');
const userRoutes = require('./routes/user.routes');
const teacherRoutes = require('./routes/teacher.routes');
const followRoutes = require('./routes/follow.routes');

const errors = require('./errors');

app.use('/api', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/teachers', teacherRoutes);
app.use('/api/courses', courseRoutes);
app.use('/api', lessonRoutes);
app.use('/api', exerciseRoutes);
app.use('/api', followRoutes);

// catch all not found response
app.use('*', function(_, res) {
  res.status(404).end()
})

// error responses
app.use('*', function(err, req, res, next) {
  console.error(`${err.name}: ${err.message}`)
  // console.error(err)
  next(err)
})

app.use('*', errors.handlers)

app.use('*', function(err, req, res, next) {
  res.status(500).json({
      message: 'something really unexpected happened'
  })
})

// export the app object for use elsewhere
module.exports = app
