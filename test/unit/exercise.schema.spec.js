const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Course = require('../../src/models/course.model')()

describe('lesson schema', function() {
    it('should be ok', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    question: 'Print Hello World',
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.ok
    })

    it('should reject missing exerciseNumber', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    question: 'Print Hello World',
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when exerciseNumber is to low', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: -1,
                    question: 'Print Hello World',
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing question', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when question is to short', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    question: 'Print H',
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing type of exercise', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    question: 'Print Hello World',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when type of exercise is not equal to enum', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    question: 'Print Hello World',
                    typeOfExercise: 'Opgave',
                    dueDate: '2020-09-02',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject when due date is before new school year - 2020-08-31', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    question: 'Print Hello World',
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-08-30',
                    isDone: 'false'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })

    it('should reject missing is done', async function() {
        const course = new Course({
            courseName: 'Programmeren 1',
            courseCode: 'PROG1',
            credits: 3,
            semester: '1.1',
            teachers: [{
                firstName: 'Dion',
                lastName: 'Koeze',
                email: 'd.koeze@avans.nl'
            }],
            lessons: [{
                lessonNumber: 1,
                date: '2020-09-02',
                duration: 50,
                exercises: [{
                    exerciseNumber: 1,
                    question: 'Print Hello World',
                    typeOfExercise: 'Programeeropgave',
                    dueDate: '2020-09-02'
                }],
                subject: 'Introdutie'
            }]
        })

        await expect(new Course(course).validate()).to.be.rejectedWith(Error)
    })
})