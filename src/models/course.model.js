const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LessonSchema = require('./lesson.schema');

const getModel = require('./model_cache');

const CourseSchema = new Schema({
  courseName: {
    type: String,
    required: [true, 'Een vak heeft een naam.'],
    minlength: 3
  },
  courseCode: {
    type: String,
    required: [true, 'Een vak heeft een unieke vakcode.'],
    minlength: 3,
    uppercase: true
  },
  credits: {
    type: Number,
    required: [true, 'Een vak heeft studiepunten.'],
    validate: {
      validator: (credits) => credits > 0,
      message: 'Studiepunten moet een positief getal zijn.'
    }
  },
  semester: {
    type: String,
    required: [true, 'Een vak hoort bij een periode.'],
    enum: ['1.1', '1.2', '1.3', '1.4', '2.1', '2.2', '2.3', '2.4', '3.3', '3.4']
  },
  teacher: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'teacher', 
    autopopulate: true
  },
  lessons: {
    type: [LessonSchema],
    default: []
  },
  creator: { type: mongoose.Schema.Types.ObjectId, ref: 'user', autopopulate: true}
}, {
  toObject: {virtuals: true},
  toJSON: {virtuals: true}
})

LessonSchema.virtual('totalPercentageOfDone').get(function () {
  if(this.lessons == undefined) {
      return "Er zijn geen lessen."
  } else {
      return this.lessons.length
  }
})

CourseSchema.plugin(require('mongoose-autopopulate'));

module.exports = getModel('course', CourseSchema);
