const chai = require('chai')
const expect = chai.expect

const requester = require('../requester.spec')

const Course = require('../../src/models/course.model')()

describe('course endpoints', function() {
    
    it('(POST /api/courses) should create a course', async function() {
            const testCourse = {
                courseName: 'Programmeren 1',
                courseCode: 'PROG1',
                credits: 3,
                semester: '1.1',
                teachers: [{
                    _id: '5fd1260fccc26439dca84114',
                    firstName: 'Dion',
                    lastName: 'Koeze',
                    email: 'd.koeze@avans.nl'
                }]
            }

            const res = await requester.post('/api/courses').send(testCourse)

            expect(res).to.have.status(201)
            expect(res.body).to.have.property('id')

            
            const course = await Course.findOne({courseName: testCourse.courseName})
            expect(course).to.have.property('courseName', testCourse.courseName)
            expect(course).to.have.property('courseCode', testCourse.courseCode)
            expect(course).to.have.property('credits', testCourse.credits)
            expect(course).to.have.property('semester', testCourse.semester)
            expect(course).to.have.property('lessons').and.to.be.empty
        })

    it('(POST /api/courses) should not create a course with missing courseName', async function() {
            const testCourse = {
                courseCode: 'PROG1',
                credits: 3,
                semester: '1.1',
                teachers: [{
                    _id: '5fd1260fccc26439dca84114',
                    firstName: 'Dion',
                    lastName: 'Koeze',
                    email: 'd.koeze@avans.nl'
                }]
            }

            const res = await requester.post('/api/courses').send(testCourse)

            expect(res).to.have.status(400)
            
            const count = await Course.find().countDocuments()
            expect(count).to.equal(0)
        })
})