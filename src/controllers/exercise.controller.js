const Course = require('../models/course.model')()
const Exercise = require('../models/exercise.schema')

const neo = require('../../neo')
const errors = require('../errors')

exports.create = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    course.lessons.id(req.params.lessonId).exercises.push(req.body)
    const result = await course.save()
    res.status(201).json({
        message: 'Successfully added lesson to course!',
        result: result
     })
}

exports.getAll = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    const exercises = await course.lessons.id(req.params.lessonId).exercises

    console.log(req.params.lessonId)
    if(!exercises) {
        res.status(404).json({
            error: 'LessonId is not right.'
        })
    }

    res.status(200).send(exercises)
}

exports.getById = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    const exercise = course.lessons.id(req.params.lessonId).exercises.id(req.params.exerciseId)

    if(!exercise){
        res.status(404).json({ message: 'Exercise not found.'})
    }
    res.status(200).send(exercise)
}

// testen
exports.update = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    // const exerciseBody = new Exercise({
    //     exerciseNumber: req.body.exerciseNumber,
    //     question: req.body.question,
    //     typeOfExercise: req.body.typeOfExercise,
    //     dueDate: req.body.dueDate,
    //     isDone: req.body.isDone
    // })
    const exercise = course.lessons.id(req.params.lessonId).exercises.id(req.params.exerciseId).set(req.body)
    await course.save()
    res.status(200).json({
        message: 'Successfully updated lesson form course!',
        result: exercise
    })
}

// testen
exports.delete = async (req, res, next) => {
    const course = await Course.findById(req.params.id)
    course.lessons.id(req.params.lessonId).exercises.id(req.params.exerciseId).remove()
    course.save()
    res.status(200).json({
        message: 'Successfully deleted exercise form lesson!'
    })
}
