const chai = require('chai')
const expect = chai.expect

const requester = require('../requester.spec')

const User = require('../../src/models/user.model')()

describe('user endpoints', function() {
    it('(GET /api/users) should get all users', async function() {
        await createMockUsers()

        const res = await requester.get('/api/users')

        expect(res).to.have.status(200)

        const count = await User.find().countDocuments()
        expect(count).to.equal(2)
    })

    it('GET to /users/:userId should return a specific user', async function () {
        await createMockUsers()

        const user = await User.findOne({ email: 'admin@test.nl' })

        const res = await requester.get('/api/users/' + user._id)

        expect(res).to.have.status(200)
        expect(user).to.have.property('firstName', user.firstName)
      })

})

async function createMockUsers() {
    await requester.post('/api/register')
      .send({
        email: 'admin@test.nl',
        firstName: 'test',
        lastName: 'test',
        password: 'appel123'
      })
  
    await requester.post('/api/register')
      .send({
        email: 'admin1@test.nl',
        firstName: 'test',
        lastName: 'test',
        password: 'banaan123'
      })
  }
  