require('dotenv').config()

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const neo = require('../../neo')

const User = require('../models/user.model')()

exports.register = async (req, res, next) => {
    const hashedPassword = await bcrypt.hash(req.body.password, 10)

    const user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPassword
    })

    const createdUser = await user.save()

    // const session = neo.session()
    // await session.run(neo.createUser, {
    //     userId: user._id.toString(),
    //     userEmail: user.email,
    //     userFirstName: user.firstName,
    //     userLastName: user.lastName
    // })
    // session.close()

    res.status(201).json({
        message: 'User created succesful',
        user: createdUser
    })
}

exports.login = async (req, res, next) => {
    let fetchedUser;
    await User.findOne({ email: req.body.email })
        .then(user => {
            if (!user){
               return res.status(401).json({
                    message: 'Auth failed, no user found with email.'
                })
            }
            fetchedUser = user;
            return bcrypt.compare(req.body.password, user.password);
        })
        .then(result => {
            if(!result) {
                return res.status(401).json({
                    message: 'Auth failed, password is incorrect.'
                })
            }
            const token = jwt.sign(
                {email: fetchedUser.email, userId: fetchedUser._id},
                process.env.JWTSECRETKEY,
                { expiresIn: '1h'}
            )
            res.status(200).json({
                token: token,
                // expiresIn: 3600,
                expiresIn: 3600000,
                user: fetchedUser
            })
        })
        .catch(err => {
            return res.status(401).json({
                message: 'Auth failed'
            })
        })
}