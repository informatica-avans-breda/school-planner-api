const mongoose = require('mongoose')
const neo_driver = require('./neo')

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}

async function mongo(dbName) {
    try{
        await mongoose.connect(`${process.env.MONGODB_URL}/${dbName}${process.env.MONGO_OPTIONS}`, options)
        console.log('Connected to MongoDB.')
    } catch (err) {
        console.log('MongoDB connection failed.')
        console.error(err)
    }
}

async function neo(url, password) {
    try{
        neo_driver.connect(url, password)
        console.log('Successfully connected to Neo4j.')
    } catch (err) {
        console.log('Neo4j connection failed.')
        console.error(err)
    }
}

module.exports = {
    mongo,
    neo
}