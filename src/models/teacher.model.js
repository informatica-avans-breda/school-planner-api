const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')

const getModel = require('./model_cache')

const TeacherSchema = new Schema({
  firstName: {
    type: String,
    required: [true, 'Een docent moet een voornaam hebben.'],
    minlength: 2
  },
  lastName: {
    type: String,
    required: [true, 'Een docent moet een achternaam hebben.'],
    minlength: 3
  },
  email: {
    type: String,
    required: [true, 'Een docent moet een emailadres hebben.'],
    // unique: [true, 'Het emailadres moet uniek zijn.']
  }
})

TeacherSchema.plugin(uniqueValidator)

module.exports = getModel('teacher', TeacherSchema)
